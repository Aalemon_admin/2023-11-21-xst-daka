import java.util.ArrayList;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Tools tools = new Tools();

        //获取统计分
        ArrayList<String> restData = new ArrayList<>();
        //查询赋值
        restData =  tools.getRestData("2024-02-05", "2024-02-11");
        //测试修改
        //循环输出
        for (String str: restData) {
            System.out.println(str);
        }

        // 调用生成数据的方法
//       tools.getInsert("2024-02-11");
    }
}