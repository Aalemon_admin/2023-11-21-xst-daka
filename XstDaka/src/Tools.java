import java.util.ArrayList;
import java.util.Collection;

public class Tools {
    /**
     * 生成获取结果的语句的方法
     * 根据传入的数值，进行SQL的赋值
     * start 开始时间
     * end 结束时间
     *
     * @return
     */
    public ArrayList<String> getRestData(String start, String end) {
        //设置返回集合
        ArrayList<String> res = new ArrayList<>();
        //设置所有JR的名字
        ArrayList<String> arr = new ArrayList<>();
        arr.add("约翰");
        arr.add("绵羊");
        arr.add("彼得");
        arr.add("小白");
        arr.add("小雅各");
        arr.add("志勇");
        arr.add("不卡");
        arr.add("盼盼");
//        arr.add("恩惠");
        //开始写输出SQL
        for (String str :
                arr) {
            //按照时间段来查询，各个打卡项目的总分
            res.add("SELECT `NAME` 昵称,PROJECT 打卡事项,SUM(SCORE) 总分 FROM xtxd_grade2 WHERE  `NAME` = '" + str + "' AND INSERT_DATE BETWEEN '" + start + "'  AND '" + end + "' GROUP BY  PROJECT;");
            //统计总分
            res.add("SELECT  SUM(总分) '" + str + "的总分' FROM (SELECT `NAME` 昵称,PROJECT 打卡事项,SUM(SCORE) 总分 FROM xtxd_grade2 WHERE  `NAME` = '" + str + "' AND INSERT_DATE BETWEEN '" + start + "'  AND '" + end + "' GROUP BY  PROJECT) T1;");
        }
        //按照总分排名
        res.add("SELECT  * FROM(SELECT SUM(SCORE) 总分,`NAME` 昵称 FROM  daka.xtxd_grade2 WHERE INSERT_DATE BETWEEN '"+start+"'  AND '"+end+"'  GROUP BY `NAME` ) AS TB1 ORDER BY TB1.`总分` DESC;");

        return res;
    }

    /**
     * 生成插入数据的SQL
     *
     * @param i     数据类型
     * @param time  计划时间
     * @param array 完成计划人员名字
     * @return
     */
    public ArrayList<String> getInsertData(int i, String time, ArrayList<String> array) {
        //储存要添加的例外数据【针对忘记打卡，但是已经完成计划的JRM】
        if(array.size()>0){
            //针对所有的计划完成都进行添加
            /*if(!this.ifcontainString(array,"不卡")){
                array.add("不卡");
            }*/
            //设置返回集合
            ArrayList<String> res = new ArrayList<>();
            //设置项目名称
            String project = "";
            //定义分数
            int f = 1;
            //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
            if (i == 0 || i == 1|| i == 8 || i == 11) {
                //设置分数
                f = 2;
                //设置类型名称
                if (i == 0) project = "善恶区分";
                else if (i == 8) project = "传道";
                else if (i == 11) project = "教藉簿2";
                else project = "练讲";
            } else {
                //设置分数
                f = 1;
                //设置类型名称
                if (i == 2) project = "礼拜感悟";
                else if (i == 3) project = "祷告";
                else if (i == 4) project = "语录";
                else if (i == 5) project = "读经";
                else if (i == 6) project = "周记";
                else if (i == 7) project = "作业复习";
                else if (i == 9) project = "之前的计划完成";
                else if (i == 10) project = "教藉簿1";
            }
            for (String str :
                    array) {
                res.add("INSERT INTO `daka`.`xtxd_grade2` (`NAME`, `INSERT_DATE`, `PROJECT`, `SCORE`) VALUES ('" + str + "', '" + time + "', '" + project + "', " + f + ");");
            }

            return res;
        }else {
            return new ArrayList<>();
        }

    }

    /**
     *
     * 打卡记分规则：
     * 背诵2分，读经1分，练剑3分，祷告1分（常规一天 7分 [ 满分 ]）
     *
     * 特殊内容记分：
     * 1、LB感悟2分
     * 2、周记或善恶区分（每周任提交一项 [ 2分 ] ）
     * 3、ZSX 3分
     *
     * 新增 模拟考试 2分，CD 2分
     *
     * @param i
     * @param time
     * @param array
     * @return
     */
    //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX 8、模拟考试 9、CD  10、背诵QSL十分钟
    public ArrayList<String> getInsertData2(int i, String time, ArrayList<String> array) {
        //储存要添加的例外数据【针对忘记打卡，但是已经完成计划的JRM】
        if(array.size()>0){
            //针对所有的计划完成都进行添加
            /*if(!this.ifcontainString(array,"不卡")){
                array.add("不卡");
            }*/
            //设置返回集合
            ArrayList<String> res = new ArrayList<>();
            //设置项目名称
            String project = "";
            //定义分数
            int f = 1;
            //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX

                //设置类型名称
                if (i == 0) {
                    project = "背诵经文";
                    f = 1;
                }
                else  if (i == 1) {
                    project = "背诵语录";
                    f = 1;
                } else  if (i == 2) {
                    project = "读经";
                    f = 1;
                } else  if (i == 3) {
                    project = "练剑";
                    f = 3;
                } else  if (i == 4) {
                    project = "祷告";
                    f = 1;
                } else  if (i == 5) {
                    project = "礼拜感悟";
                    f = 2;
                } else  if (i == 6) {
                    project = "周记or善恶区分";
                    f = 2;
                } else  if (i == 7) {
                    project = "ZSX";
                    f = 3;
                } else  if (i == 8) {
                    project = "模拟考试";
                    f = 2;
                } else  if (i == 9) {
                    project = "CD";
                    f = 2;
                }else  if (i == 10) {
                    project = "背诵QSL十分钟";
                    f = 2;
                }

            for (String str :
                    array) {
                res.add("INSERT INTO `daka`.`xtxd_grade2` (`NAME`, `INSERT_DATE`, `PROJECT`, `SCORE`) VALUES ('" + str + "', '" + time + "', '" + project + "', " + f + ");");
            }

            return res;
        }else {
            return new ArrayList<>();
        }

    }

    /**
     * 判断集合中是否存在某个字符串  限制String类型的list
     *
     * @param list
     * @param str
     * @return
     */
    public boolean ifcontainString(Collection<?> list, String str) {
        boolean b = false;
        for (Object o : list) {
            if (o.equals(str)) {
                return b = true;
            }
        }
        return b;
    }


    public void getInsert(String time){
        //0、背诵经文
        String s = "";
        //1、背诵语录
        String s1 = "";
        //2、读经
        String s2 = "约翰 彼得 绵羊";
        //3、练剑
        String s3 ="";
        //4、祷告
        String s4 = "彼得 绵羊";
        //5、礼拜感悟
        String s5 = "";
        //6、周记or善恶区分
        String s6 = "";
        //7、ZSX
        String s7 = "";
        //8、模拟考试
        String s8 = "";
        //9、CD
        String s9 = "约翰 彼得 绵羊 志勇";
        //10、背诵QSL十分钟
        String s10 = "";
        //分隔
        String[] result = s.split(" ");
        String[] result1 = s1.split(" ");
        String[] result2 = s2.split(" ");
        String[] result3 = s3.split(" ");
        String[] result4 = s4.split(" ");
        String[] result5 = s5.split(" ");
        String[] result6 = s6.split(" ");
        String[] result7 = s7.split(" ");
        String[] result8 = s8.split(" ");
        String[] result9 = s9.split(" ");
        String[] result10 = s10.split(" ");

        //声明一个工具类的对象
        Tools tools = new Tools();
        //背诵经文
        ArrayList<String> arr= new ArrayList<>();
        //判断不是空，再截取
        if(s != ""){
            for (int i=0; i<result.length; i++) {
                arr.add(result[i]);
            }
        }

        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData = tools.getInsertData2(0, time, arr);
        for (String str:
                insertData) {
            System.out.println(str);
        }
        System.out.println();

        //背诵语录
        ArrayList<String> arr1= new ArrayList<>();
        //判断不是空，再截取
        if(s1 != ""){
            for (int i=0; i<result1.length; i++) {
                arr1.add(result1[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData1 = tools.getInsertData2(1, time, arr1);
        for (String str:
                insertData1) {
            System.out.println(str);
        }
        System.out.println();

        //读经
        ArrayList<String> arr2= new ArrayList<>();
        //判断不是空，再截取
        if(s2 != ""){
            for (int i=0; i<result2.length; i++) {
                arr2.add(result2[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData2 = tools.getInsertData2(2, time, arr2);
        for (String str:
                insertData2) {
            System.out.println(str);
        }
        System.out.println();


        //练剑
        ArrayList<String> arr3= new ArrayList<>();
        //判断不是空，再截取
        if(s3 != ""){
            for (int i=0; i<result3.length; i++) {
                arr3.add(result3[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData3 = tools.getInsertData2(3, time, arr3);
        for (String str:
                insertData3) {
            System.out.println(str);
        }
        System.out.println();


        //祷告
        ArrayList<String> arr4= new ArrayList<>();
        //判断不是空，再截取
        if(s4 != ""){
            for (int i=0; i<result4.length; i++) {
                arr4.add(result4[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData4 = tools.getInsertData2(4, time, arr4);
        for (String str:
                insertData4) {
            System.out.println(str);
        }
        System.out.println();

        //礼拜感悟
        ArrayList<String> arr5= new ArrayList<>();
        //判断不是空，再截取
        if(s5 != ""){
            for (int i=0; i<result5.length; i++) {
                arr5.add(result5[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData5 = tools.getInsertData2(5, time, arr5);
        for (String str:
                insertData5) {
            System.out.println(str);
        }
        System.out.println();

        //周记or善恶区分
        ArrayList<String> arr6= new ArrayList<>();
        //判断不是空，再截取
        if(s6 != ""){
            for (int i=0; i<result6.length; i++) {
                arr6.add(result6[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData6 = tools.getInsertData2(6, time, arr6);
        for (String str:
                insertData6) {
            System.out.println(str);
        }
        System.out.println();

        //ZSX
        ArrayList<String> arr7= new ArrayList<>();
        //判断不是空，再截取
        if(s7 != ""){
            for (int i=0; i<result7.length; i++) {
                arr7.add(result7[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX
        ArrayList<String> insertData7 = tools.getInsertData2(7, time, arr7);
        for (String str:
                insertData7) {
            System.out.println(str);
        }
        System.out.println();


        //模拟考试
        ArrayList<String> arr8= new ArrayList<>();
        //判断不是空，再截取
        if(s8 != ""){
            for (int i=0; i<result8.length; i++) {
                arr8.add(result8[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX 8、模拟考试 9、CD
        ArrayList<String> insertData8 = tools.getInsertData2(8, time, arr8);
        for (String str:
                insertData8) {
            System.out.println(str);
        }
        System.out.println();

        //CD
        ArrayList<String> arr9= new ArrayList<>();
        //判断不是空，再截取
        if(s9 != ""){
            for (int i=0; i<result9.length; i++) {
                arr9.add(result9[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX 8、模拟考试 9、CD 10、背诵QSL十分钟
        ArrayList<String> insertData9 = tools.getInsertData2(9, time, arr9);
        for (String str:
                insertData9) {
            System.out.println(str);
        }
        System.out.println();

        //背诵QSL十分钟
        ArrayList<String> arr10= new ArrayList<>();
        //判断不是空，再截取
        if(s10 != ""){
            for (int i=0; i<result10.length; i++) {
                arr10.add(result10[i]);
            }
        }
        //0、背诵经文  1、背诵语录 2、读经 3、练剑 4、祷告 5、礼拜感悟 6、周记or善恶区分 7、ZSX 8、模拟考试 9、CD 10、背诵QSL十分钟
        ArrayList<String> insertData10 = tools.getInsertData2(10, time, arr10);
        for (String str:
                insertData10) {
            System.out.println(str);
        }
        System.out.println();

    }

}
